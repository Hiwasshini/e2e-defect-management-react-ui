import React from 'react';

// import InputLabel from '@material-ui/core/InputLabel';
// import TextField from '@material-ui/core/TextField';
import AccordionList from './Accordion';
import { Box, Button, Container, CssBaseline, MenuItem, Select } from '@material-ui/core';

const axios = require('axios');

// const styles = theme => ({ //this is the simplified non-Hook method of defining.
//   root:
//   {
//     '& > *': {
//       margin: theme.spacing(1),
//       width: '25ch',
//     },
//   },
// })

export default class GetRequest extends React.Component {

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      //validationError: "",
      ndtstory: [], //data array
      expectedfindingtype: [],
      assemblycondition: [],
      inspectionmethod: [],
      selectedNDTStory: "",
      selectedFindingType: "",
      selectedAssemblyCondition: "",
      selectedInspectionMethod: "",
      aircrafttype: '',
      airlines: '',
      msn: '',
      submit: '',
      inspectionrequest: [],
      inspectionarea: [],
      inspectionareacycle: [],
      inspectionfinding: []
    };
  }

  componentDidMount() {
    axios.get('http://localhost:3001/api/e2e/ndtstory', {}) //pulling data from NDT Story
      .then(response => {
        //const data = response.data
        // console.log(response.data.Result)
        this.setState({
          ndtstory: response.data.Result //set incoming data into ndtstory array
        })
      })
    axios.get('http://localhost:3001/api/e2e/findingtype', {}) //pulling data from Finding Type
      .then(response => {
        // console.log(response.data.Result)
        this.setState({
          expectedfindingtype: response.data.Result //set incoming data into data array
        })
      })
    axios.get('http://localhost:3001/api/e2e/assemblycondition', {}) //pulling data from Assembly Condition
      .then(response => {
        //console.log(response.data.Result)
        this.setState({
          assemblycondition: response.data.Result //set incoming data into data array
        })
      })
    axios.get('http://localhost:3001/api/e2e/inspectionmethod', {}) //pulling data from Inspection Method
      .then(response => {
        //console.log(response.data.Result)
        this.setState({
          inspectionmethod: response.data.Result //set incoming data into data array
        })
      })
  }

  getInspectionRequest = () => {
    //check the Network tab to see if the request is correct
    axios.get('http://localhost:3001/api/e2e/inspectionrequest',
      {
        params: { //PARAMS CAN ONLY BE USED TO APPEND QUERIES IN GET REQUESTS
          NDT_Story: this.state.selectedNDTStory,
          Aircraft_Type: this.state.aircrafttype,
          Expected_Finding_Type: this.state.selectedFindingType,
          Assembly_Condition: this.state.selectedAssemblyCondition,
          Inspection_Method: this.state.selectedInspectionMethod,
          Airlines: this.state.airlines,
          MSN: this.state.msn
        }
      }
    ) //pulling data from inspection request and setting the state below
      .then(response => {
        //console.log(response.data[0].NDT_Inspection_Area)
        this.setState({
          inspectionrequest: response.data,//set incoming data into data array
          inspectionarea: response.data[0].NDT_Inspection_Area,//this inspection area only reads from the first array of inspection request. Needs to be modified to read from the multiple arrays! 
          inspectionareacycle: response.data[0].NDT_Inspection_Area[0].NDT_Inspection_Area_Cycle,//this inspection area cycle only reads from the first array of inspection area. Needs to be modified to read from the multiple arrays! 
          inspectionfinding: response.data[0].NDT_Inspection_Area[0].NDT_Inspection_Area_Cycle[0].NDT_Inspection_Finding//this inspection finding only reads from the first array of inspection area cycle. Needs to be modified to read from the multiple arrays! 
        })
      }).catch((error) => {
        console.log(error)
      })
  }

  handleChange(event) {
    this.setState({ submit: event.target.value });
  }

  handleSubmit(event) {
    alert('Searching the database...');
    event.preventDefault();
    //console.log(this.state.selectedNDTStory)
    this.getInspectionRequest() //this function runs when the button Search is clicked
  }
  handleSubmit1(event) {
    event.preventDefault();
    this.props.history.push('/LoginPage')
  }

  renderOptions() {
    return this.state.ndtstory.map((dt, i) => { //mapping data array to get each set of data
      // console.log(dt);
      return dt.map(j => { //mapping each set of data to get NDT_Story Value only
        return (
          <MenuItem
            label="Select a NDT Story"
            value={j.NDT_Story}
            key={i} name={j.NDT_Story}>{j.NDT_Story}</MenuItem>

        );
      });
    });
  }
  renderOptions1() {
    return this.state.expectedfindingtype.map((dt, i) => { //mapping data array to get each set of data
      return dt.map(j => { //mapping each set of data to get Finding_Type Value only
        return (
          <MenuItem
            label="Select a Expected Finding Type"
            value={j.Finding_Type}
            key={i} name={j.Finding_Type}>{j.Finding_Type}</MenuItem>
        );
      });
    });
  }
  renderOptions2() {
    return this.state.assemblycondition.map((dt, i) => { //mapping data array to get each set of data
      return dt.map(j => { //mapping each set of data to get Assembly_Condition Value only
        return (
          <MenuItem
            label="Select a Assembly Condition"
            value={j.Assembly_Condition}
            key={i} name={j.Assembly_Condition}>{j.Assembly_Condition}</MenuItem>
        );
      });
    });
  }
  renderOptions3() {
    return this.state.inspectionmethod.map((dt, i) => { //mapping data array to get each set of data
      return dt.map(j => { //mapping each set of data to get Inspection_Method Value only
        return (
          <MenuItem
            label="Select a Inspection Method"
            value={j.Inspection_Method}
            key={i} name={j.Inspection_Method}>{j.Inspection_Method}</MenuItem>
        );
      });
    });
  }

  //note: do not include functions in render; it will corrupt the page load
  //Select => dropdown list, input => textfield
  render() { //title and components
    // console.log(this.state.selectedNDTStory);
    return (
      <div
        className="padd50"><Container >
          <CssBaseline />
          <Box pb={15} /><h1>Retrieve records from Inspection Request</h1>
          <h3>NDT Story</h3>
          <Select
            className="width50" value={this.state.selectedNDTStory} onChange={(e) => this.setState({ selectedNDTStory: e.target.value })}>
            {this.renderOptions()}
          </Select>
          <form>
            <label>
              <h3> Aircraft Type: </h3>
              <input type="text" value={this.state.aircrafttype} onChange={(e) => this.setState({ aircrafttype: e.target.value })} />
            </label>
          </form>
          <h3>Expected Finding Type</h3>
          <Select
            className="width50" value={this.state.selectedFindingType} onChange={(e) => this.setState({ selectedFindingType: e.target.value })}>
            {this.renderOptions1()}
          </Select>
          <h3>Assembly Condition</h3>
          <Select
            className="width50" value={this.state.selectedAssemblyCondition} onChange={(e) => this.setState({ selectedAssemblyCondition: e.target.value })}>
            {this.renderOptions2()}
          </Select>
          <h3>Inspection Method</h3>
          <Select
            className="width50" value={this.state.selectedInspectionMethod} onChange={(e) => this.setState({ selectedInspectionMethod: e.target.value })}>
            {this.renderOptions3()}
          </Select>
          <form>
            <label>
              <h3> Airlines: </h3>
              <input type="text" value={this.state.airlines} onChange={(e) => this.setState({ airlines: e.target.value })} />
            </label>
            <label>
              <h3> MSN: </h3>
              <input type="text" value={this.state.msn} onChange={(e) => this.setState({ msn: e.target.value })} />
            </label>
          </form>
          <form onSubmit={this.handleSubmit}>
            <label>
              <p><Button type="submit" onChange={this.handleChange} variant="contained" color="primary">Search</Button></p>
              <p><Button type="submit" onClick={(e) => this.handleSubmit1(e)} variant="contained" color="secondary">Back to Login</Button></p>
            </label>
            {/* Passing state as props to Accordion.js */}
            <AccordionList NDT_Story={this.state.selectedNDTStory}
              Aircraft_Type={this.state.aircrafttype}
              Expected_Finding_Type={this.state.selectedFindingType}
              Assembly_Condition={this.state.selectedAssemblyCondition}
              Inspection_Method={this.state.selectedInspectionMethod}
              Airlines={this.state.airlines}
              MSN={this.state.msn}
              ir_data={this.state.inspectionrequest}
              ia_data={this.state.inspectionarea}
              iac_data={this.state.inspectionareacycle}
              if_data={this.state.inspectionfinding}
            />
          </form> </Container>
      </div>
    );
  }
}
