//Copy of GetRequest: needs to be modified for the POST request

import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select';
// import InputLabel from '@material-ui/core/InputLabel';
// import TextField from '@material-ui/core/TextField';
import InspectionRequest from './Accordion';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';

const axios = require('axios');

// const styles = theme => ({ //this is the simplified non-Hook method of defining.
//   root:
//   {
//     '& > *': {
//       margin: theme.spacing(1),
//       width: '25ch',
//     },
//   },
// })

export default class GetRequest extends React.Component {

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      ndtstory: [], //data array
      selectedNDTStory: "",
      //validationError: "",
      aircrafttype: '',
      expectedfindingtype: [], 
      selectedFindingType: "",
      assemblycondition: [], 
      selectedAssemblyCondition: "",
      inspectionmethod: [], 
      selectedInspectionMethod: "",
      airlines: '',
      msn: '',
      submit: '',
      inspectionrequest: [], 
      inspectionarea: [],
      inspectionareacycle: [],
      inspectionfinding: []
    };
  }
  // getNDTStory = () => {
  componentDidMount() {
    axios.get('http://localhost:3001/api/e2e/ndtstory', {}) //pulling data from NDT Story
      .then(response => {
        //const data = response.data
        // console.log(response.data.Result)
        this.setState({
          ndtstory: response.data.Result //set incoming data into ndtstory array
        })
      })
    axios.get('http://localhost:3001/api/e2e/findingtype', {}) //pulling data from Finding Type
      .then(response => {
        //const data = response.data
        // console.log(response.data.Result)
        this.setState({
          expectedfindingtype: response.data.Result //set incoming data into data array
        })
      })
    axios.get('http://localhost:3001/api/e2e/assemblycondition', {}) //pulling data from Assembly Condition
      .then(response => {
        //const data = response.data
        //console.log(response.data.Result)
        this.setState({
          assemblycondition: response.data.Result //set incoming data into data array
        })
      })
    axios.get('http://localhost:3001/api/e2e/inspectionmethod', {}) //pulling data from Inspection Method
      .then(response => {
        //const data = response.data
        //console.log(response.data.Result)
        this.setState({
          inspectionmethod: response.data.Result //set incoming data into data array
        })
      })
  }

  getInspectionRequest = () => {
    //componentWillMount() {
    axios.get('http://localhost:3001/api/e2e/inspectionrequest',
      {
        params: { //PARAMS CAN ONLY BE USED TO APPEND QUERIES IN GET REQUESTS
          NDT_Story: this.state.selectedNDTStory,
          //  Aircraft_Type: this.state.aircrafttype,
          // Expected_Finding_Type: this.state.selectedFindingType,
          // Assembly_Condition: this.state.selectedAssemblyCondition,
          // Inspection_Method: this.state.selectedInspectionMethod,
          // Airlines: this.state.airlines,
          // MSN: this.state.msn
        }
      }
    ) //pulling data from inspection request
      .then(response => {
        console.log(response.data[0].NDT_Inspection_Area)
        //console.log(JSON.stringify(response.data).concat( response.data))
        this.setState({
          inspectionrequest: response.data,//set incoming data into data array
          inspectionarea: response.data[0].NDT_Inspection_Area,
          inspectionareacycle: response.data[0].NDT_Inspection_Area[0].NDT_Inspection_Area_Cycle,
          inspectionfinding: response.data[0].NDT_Inspection_Area[0].NDT_Inspection_Area_Cycle[0].NDT_Inspection_Finding
        })
      }).catch((error) => {
        console.log(error)
      })
  }

  handleChange7(event) {
    this.setState({ submit: event.target.value });
  }

  handleSubmit(event) {
    //let event.target.value = x;
    alert('Searching the database...');
    event.preventDefault();
    console.log(this.state.selectedNDTStory)
    this.getInspectionRequest()
    // this.state.selectedNDTStory('/InspectionRequest')
    //this.handleChange(event);
    // this.props.history.push('/InspectionRequest')
  }
  handleSubmit1(event) {
    event.preventDefault();
     this.props.history.push('/LoginPage')
  }

  renderOptions() {
    return this.state.ndtstory.map((dt, i) => { //mapping data array to get each set of data
      // console.log(dt);
      return dt.map(j => { //mapping each set of data to get NDT_Story
        return (
          <MenuItem
            label="Select a NDT Story"
            value={j.NDT_Story}
            key={i} name={j.NDT_Story}>{j.NDT_Story}</MenuItem>

        );
      });
    });
  }
  renderOptions1() {
    return this.state.expectedfindingtype.map((dt, i) => { //mapping data array to get each set of data
      // console.log(dt);
      return dt.map(j => { //mapping each set of data to get Finding_Type
        return (
          <MenuItem
            label="Select a Expected Finding Type"
            value={j.Finding_Type}
            key={i} name={j.Finding_Type}>{j.Finding_Type}</MenuItem>
        );
      });
    });
  }
  renderOptions2() {
    return this.state.assemblycondition.map((dt, i) => { //mapping data array to get each set of data
      // console.log(dt);
      return dt.map(j => { //mapping each set of data to get Assembly_Condition
        return (
          <MenuItem
            label="Select a Assembly Condition"
            value={j.Assembly_Condition}
            key={i} name={j.Assembly_Condition}>{j.Assembly_Condition}</MenuItem>
        );
      });
    });
  }
  renderOptions3() {
    return this.state.inspectionmethod.map((dt, i) => { //mapping data array to get each set of data
      // console.log(dt);
      return dt.map(j => { //mapping each set of data to get Inspection_Method
        return (
          <MenuItem
            label="Select a Inspection Method"
            value={j.Inspection_Method}
            key={i} name={j.Inspection_Method}>{j.Inspection_Method}</MenuItem>
        );
      });
    });
  }



  render() { //title and components
    // console.log(this.state.selectedNDTStory);
    return (
      <div  
      className="padd50"><Container >
      <CssBaseline />
      <Box  pb={15}/><h1>Post records to Inspection Request</h1>
        <h3>NDT Story</h3>
        <Select
          className="width50"  value={this.state.selectedNDTStory} onChange={(e)=>this.setState({ selectedNDTStory: e.target.value })}>
        {this.renderOptions()}
        </Select>
        <form>
          <label>
            <h3> Aircraft Type: </h3>
            <input type="text" value={this.state.aircrafttype} onChange={(e)=>this.setState({ aircrafttype: e.target.value })} />
          </label>
        </form>
        <h3>Expected Finding Type</h3>
        {/* {this.getExpectedFindingType()} */}
        <Select
          className="width50" value={this.state.selectedFindingType} onChange={(e)=>this.setState({ selectedFindingType: e.target.value })}>
          {this.renderOptions1()}
        </Select>
        <h3>Assembly Condition</h3>
        {/* {this.getAssemblyCondition()} */}
        <Select
          className="width50" value={this.state.selectedAssemblyCondition} onChange={(e)=>this.setState({ selectedAssemblyCondition: e.target.value })}>
          {this.renderOptions2()}
        </Select>
        <h3>Inspection Method</h3>
        {/* {this.getInspectionMethod()} */}
        <Select
          className="width50" value={this.state.selectedInspectionMethod} onChange={(e)=>this.setState({ selectedInspectionMethod: e.target.value })}>
          {this.renderOptions3()}
        </Select>
        <form>
          <label>
            <h3> Airlines: </h3>
            <input type="text" value={this.state.airlines} onChange={(e)=>this.setState({ airlines: e.target.value })} />
          </label>
          <label>
            <h3> MSN: </h3>
            <input type="text" value={this.state.msn} onChange={(e)=>this.setState({ msn: e.target.value })} />
          </label>
        </form>
        <form onSubmit={this.handleSubmit}>
          <label>
            <p><Button type="submit" onChange={this.handleChange7} variant="contained" color="primary">Post</Button></p>
            <p><Button type="submit" onClick={(e) => this.handleSubmit1(e)} variant="contained" color="secondary">Back to Login</Button></p>
          </label>
          <InspectionRequest NDT_Story={this.state.selectedNDTStory}
            Aircraft_Type={this.state.aircrafttype}
            Expected_Finding_Type={this.state.selectedFindingType}
            Assembly_Condition={this.state.selectedAssemblyCondition} 
            Inspection_Method={this.state.selectedInspectionMethod}
            Airlines={this.state.airlines}
            MSN={this.state.msn}
            ir_data={this.state.inspectionrequest}
            ia_data={this.state.inspectionarea}
            iac_data={this.state.inspectionareacycle}
            if_data={this.state.inspectionfinding}
          />
        </form> </Container>
      </div>
    );
  }
}
