
import React from 'react';
import { Accordion, AccordionSummary, AccordionDetails, Typography, Divider } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  root:
  {
    table: {
      minWidth: 650,
    }
  },
  content: {
    flexGrow: 1,
    padding: "0.5rem",
  },
})
const classes = styles();

export default class AccordionList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      dataRows: []
    }
  };

  // cleans the ir_data before being passed to accordion
  createData = () => {
    var dataRowsTemp = [];
    //console.log(this.props)
    //instead of multiple if-else if statements to check whether props is being passed before proceeding, just do this.props
    if (this.props !== null) {
      //var dataRowsTemp = this.state.dataRows.slice();  // NEVER modify state directly. Slice creates a shallow copy , if u want to APPEND/MODIFY to existing state
      this.props.ir_data.map(value => {
        dataRowsTemp.push(
          // this.createData(value)
          value
        )
      })
      this.setState({ dataRows: dataRowsTemp }, this.callbackExample) //always use setState to change state value
      //this.callbackExample(); //DONT DO THIS. setState does not always run on the next immediate cycle and you may end up reading in old values.Use callback as show above
    }
    // else if (this.props.Aircraft_Type !== null) {
    //   this.props.ir_data.map(value => {
    //     dataRowsTemp.push(
    //       value
    //     )
    //   })
    //   this.setState({ dataRows: dataRowsTemp }, this.callbackExample)
    // }
    // else if (this.props.Expected_Finding_Type !== null) {
    //   this.props.ir_data.map(value => {
    //     dataRowsTemp.push(
    //       value
    //     )
    //   })
    //   this.setState({ dataRows: dataRowsTemp }, this.callbackExample)
    // }
    // else if (this.props.Assembly_Condition !== null) {
    //   this.props.ir_data.map(value => {
    //     dataRowsTemp.push(
    //       value
    //     )
    //   })
    //   this.setState({ dataRows: dataRowsTemp }, this.callbackExample)
    // }
    // else if (this.props.Inspection_Method !== null) {
    //   this.props.ir_data.map(value => {
    //     dataRowsTemp.push(
    //       value
    //     )
    //   })
    //   this.setState({ dataRows: dataRowsTemp }, this.callbackExample)
    // }
    // else if (this.props.Airlines !== null) {
    //   this.props.ir_data.map(value => {
    //     dataRowsTemp.push(
    //       value
    //     )
    //   })
    //   this.setState({ dataRows: dataRowsTemp }, this.callbackExample)
    // }
    // else if (this.props.MSN !== null) {
    //   this.props.ir_data.map(value => {
    //     dataRowsTemp.push(
    //       value
    //     )
    //   })
    //   this.setState({ dataRows: dataRowsTemp }, this.callbackExample)
    // }
  }


  //this example is to illustrate 2 things:
  // 1) the use of a callback function. This provides a function to be called by ASYNC functions ONCE they are done
  // 2) To illustrate the use of the setState callback to only run when the state is properly updated
  callbackExample() {
    console.log(this.state.dataRows)
    //console.log(this.state.inspectionrequest)
  }

  // componentDidMount() { //runs when the program is executed
  //   this.createData();
  // }

  //runs when there is a change in ir_data in a component (state/props)
  componentDidUpdate(prevProps) {
    if (prevProps.NDT_Story !== this.props.NDT_Story) //we check here if the latest change in ir_data is different from the prev ir_data values
    {
      this.createData();
    }
    else if (prevProps.Aircraft_Type !== this.props.Aircraft_Type) {
      this.createData();
    }
    else if (prevProps.Expected_Finding_Type !== this.props.Expected_Finding_Type) {
      this.createData();
    }
    else if (prevProps.Assembly_Condition !== this.props.Assembly_Condition) {
      this.createData();
    }
    else if (prevProps.Inspection_Method !== this.props.Inspection_Method) {
      this.createData();
    }
    else if (prevProps.Airlines !== this.props.Airlines) {
      this.createData();
    }
    else if (prevProps.MSN !== this.props.MSN) {
      this.createData();
    }
  }

  render() {
    //console.log(this.props.MSN)
    //console.log(this.renderOptions)
    return (
      <div className={classes.root} >
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon color="primary" />}
            aria-controls="panel1a-content"
            id="panel1a-header">
            <Typography className={classes.heading} color="primary">Inspection Request</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              {/* {this.props.NDT_Story} */}
              {/* {JSON.parse(JSON.stringify(this.state.dataRows))} */}
              {this.state.dataRows.map((dt, i) => { //mapping ir_data array to get each set of ir_data
                console.log(dt);
                {
                  if (this.state.dataRows !== null) {
                    { console.log(this.state.dataRows) }
                    return (
                      <div value={dt.inspectionrequest} key={i}>
                        <Divider /><Typography
                          className={classes.dividerFullWidth}
                          color="Primary"
                          display="block"
                          variant="caption">
                          <h4>Inspection Request {i} </h4>
                        </Typography>
                        <p><strong>Inspection Request ID:</strong>{dt.Inspection_Request_ID}</p>
                        <p><strong>Program:</strong>{dt.Program}</p>
                        <p><strong>Aircraft Type:</strong>{dt.Aircraft_Type}</p>
                        <p><strong>Applicable MSN Range:</strong>{dt.Applicable_MSN_Range}</p>
                        <p><strong>Priority:</strong>{dt.Priority}</p>
                        <p><strong>Stress Leader:</strong>{dt.Stress_Leader}</p>
                        <p><strong>NDI Compiler:</strong>{dt.NDI_Compiler}</p>
                        <p><strong>Test Leader:</strong>{dt.Test_Leader}</p>
                        <p><strong>Requestor:</strong>{dt.Requestor}</p>
                        <p><strong>NDT Story ID:</strong>{dt.NDT_Story_ID}</p>
                        <p><strong>NDT Story:</strong>{dt.NDT_Story}</p>
                      </div>
                    )
                  }
                }
              })}
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel2a-content"
                  id="panel2a-header">
                  <Typography className={classes.heading}>Inspection Area</Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                    {this.props.ia_data.map((dt, i) => { //mapping ia_data array to get each set of ia_data
                      console.log(dt);
                      {
                        if (this.state.dataRows !== null) {
                          { console.log(this.state.dataRows) }
                          for (let index = 0; index < this.props.ia_data.length; index++) {
                            return (
                              <div> <Divider /><Typography
                                className={classes.dividerFullWidth}
                                color="Primary"
                                display="block"
                                variant="caption">
                                <h4>Inspection Area {i} </h4>
                              </Typography>
                                <p><strong>Inspection Area ID:</strong>{dt.Inspection_Area_ID}</p>
                                <p><strong>Inspection Request ID:</strong>{dt.Inspection_Request_ID}</p>
                                <p><strong>Side ID:</strong>{dt.Side_ID}</p>
                                <p><strong>Side:</strong>{dt.Side}</p>
                                <p><strong>Material:</strong>{dt.Material}</p>
                                <p><strong>Inspection Type ID:</strong>{dt.Inspection_Type_ID}</p>
                                <p><strong>Inspection Type:</strong>{dt.Inspection_Type}</p>
                                <p><strong>Area Description:</strong>{dt.Area_Description}</p>
                                <p><strong>Inspection Effort:</strong>{dt.Inspection_Effort}</p>
                                <p><strong>Inspection Method ID:</strong>{dt.Inspection_Method_ID}</p>
                                <p><strong>Inspection Method:</strong>{dt.Inspection_Method}</p>
                                <p><strong>Assembly Condition ID:</strong>{dt.Assembly_Condition_ID}</p>
                                <p><strong>Assembly Condition:</strong>{dt.Assembly_Condition}</p>
                                <p><strong>Area Inspectability ID:</strong>{dt.Area_Inspectability_ID}</p>
                                <p><strong>Area Inspectability:</strong>{dt.Area_Inspectability}</p>
                              </div>
                            )
                          }
                        }
                      }
                    })}
                    <Accordion>
                      <AccordionSummary
                        expandIcon={<ExpandMoreIcon color="primary" />}
                        aria-controls="panel2a-content"
                        id="panel2a-header">
                        <Typography className={classes.heading} color="primary">Inspection Area Cycle</Typography>
                      </AccordionSummary>
                      <AccordionDetails>
                        <Typography>
                          {this.props.iac_data.map((dt, i) => { //mapping iac_data array to get each set of iac_data
                            console.log(dt);
                            {
                              if (this.state.dataRows !== null) {
                                { console.log(this.state.dataRows) }
                                return (
                                  <div> <Divider /><Typography
                                    className={classes.dividerFullWidth}
                                    color="Primary"
                                    display="block"
                                    variant="caption">
                                    <h4>Inspection Area Cycle {i} </h4>
                                  </Typography>
                                    <p><strong>Inspection Area Cycle ID:</strong>{dt.Inspection_Area_Cycle_ID}</p>
                                    <p><strong>Inspection Area ID:</strong>{dt.Inspection_Area_ID}</p>
                                    <p><strong>MSN:</strong>{dt.MSN}</p>
                                    <p><strong>Airline ID:</strong>{dt.Airline_ID}</p>
                                    <p><strong>Airlines:</strong>{dt.Airlines}</p>
                                    <p><strong>Flying Cycle:</strong>{dt.Flying_Cycle}</p>
                                    <p><strong>Flying Hours:</strong>{dt.Flying_Hours}</p>
                                    <p><strong>Campaign Date:</strong>{dt.Campaign_Date}</p>
                                    <p><strong>Aircraft Model:</strong>{dt.Aircraft_Model}</p>
                                    <p><strong>Equipment Type ID:</strong>{dt.Equipment_Type_ID}</p>
                                    <p><strong>Geographical Area:</strong>{dt.Geographical_Area}</p>
                                    <p><strong>Geographical Area:</strong>{dt.Geographical_Area}</p>
                                    <p><strong>Name of Equipment:</strong>{dt.Name_of_Equipment}</p>
                                    <p><strong>Type of Equipment:</strong>{dt.Type_of_Equipment}</p>
                                    <p><strong>Entry Into Service:</strong>{dt.Entry_Into_Service}</p>
                                    <p><strong>Manufacturing Date:</strong>{dt.Manufacturing_Date}</p>
                                    <p><strong>Inspection Campaign:</strong>{dt.Inspection_Campaign}</p>
                                    <p><strong>Calibration Due Date:</strong>{dt.Calibration_Due_Date}</p>
                                    <p><strong>Calibration Specimen:</strong>{dt.Calibration_Specimen}</p>
                                    <p><strong>Calibration Procedure:</strong>{dt.Calibration_Procedure}</p>
                                    <p><strong>Equipment Serial Number:</strong>{dt.Equipment_Serial_Number}</p>
                                  </div>
                                )
                              }
                            }
                          })}
                          <Accordion>
                            <AccordionSummary
                              expandIcon={<ExpandMoreIcon />}
                              aria-controls="panel2a-content"
                              id="panel2a-header">
                              <Typography className={classes.heading}>Inspection Finding</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                              <Typography>
                                {this.props.if_data.map((dt, i) => { //mapping if_data array to get each set of if_data
                                  console.log(dt);
                                  {
                                    if (this.state.dataRows !== null) {
                                      { console.log(this.state.dataRows) }
                                      return (
                                        <div> <Divider /><Typography
                                          className={classes.dividerFullWidth}
                                          color="Primary"
                                          display="block"
                                          variant="caption">
                                          <h4>Inspection Finding {i} </h4>
                                        </Typography>
                                          <p><strong>Inspection Finding ID:</strong>{dt.Inspection_Finding_ID}</p>
                                          <p><strong>Inspection Area Cycle ID:</strong>{dt.Inspection_Area_Cycle_ID}</p>
                                          <p><strong>Depth:</strong>{dt.Depth}</p>
                                          <p><strong>Layer:</strong>{dt.Layer}</p>
                                          <p><strong>Width:</strong>{dt.Width}</p>
                                          <p><strong>Length:</strong>{dt.Length}</p>
                                          <p><strong>IP Name:</strong>{dt.IP_Name}</p>
                                          <p><strong>Surface:</strong>{dt.Surface}</p>
                                          <p><strong>Comments:</strong>{dt.Comments}</p>
                                          <p><strong>RMT File:</strong>{dt.RMT_File}</p>
                                          <p><strong>Number Spot:</strong>{dt.Number_Spot}</p>
                                          <p><strong>Orientation:</strong>{dt.Orientation}</p>
                                          <p><strong>Part Number:</strong>{dt.Part_Number}</p>
                                          <p><strong>Repair Type ID:</strong>{dt.Repair_Type_ID}</p>
                                          <p><strong>Repair Type:</strong>{dt.Repair_Type}</p>
                                          <p><strong>Starting Depth:</strong>{dt.Starting_Depth}</p>
                                          <p><strong>Ending Depth:</strong>{dt.Ending_Depth}</p>
                                          <p><strong>Finding Date:</strong>{dt.Finding_Date}</p>
                                          <p><strong>Finding Type ID:</strong>{dt.Finding_Type_ID}</p>
                                          <p><strong>Finding Type:</strong>{dt.Finding_Type}</p>
                                          <p><strong>SSI Reference:</strong>{dt.SSI_Reference}</p>
                                          <p><strong>Part Criticity ID:</strong>{dt.Part_Criticity_ID}</p>
                                          <p><strong>Part Criticity:</strong>{dt.Part_Criticity}</p>
                                          <p><strong>RDAS Reference:</strong>{dt.RDAS_Reference}</p>
                                          <p><strong>Rotation Angle:</strong>{dt.Rotation_Angle}</p>
                                          <p><strong>Angle Reference:</strong>{dt.Angle_Reference}</p>
                                          <p><strong>Finding Category ID:</strong>{dt.Finding_Category_ID}</p>
                                          <p><strong>Finding Category:</strong>{dt.Finding_Category}</p>
                                          <p><strong>Repair Reference:</strong>{dt.Repair_Reference}</p>
                                          <p><strong>Flying Cycle Real:</strong>{dt.Flying_Cycle_Real}</p>
                                          <p><strong>Part Serial Number:</strong>{dt.Part_Serial_Number}</p>
                                          <p><strong>EASA AMC 2020 Level ID:</strong>{dt.EASA_AMC_2020_Level_ID}</p>
                                          <p><strong>EASA AMC 2020 Level:</strong>{dt.EASA_AMC_2020_Level}</p>
                                          <p><strong>Remaining Thickness:</strong>{dt.Remaining_Thickness}</p>
                                          <p><strong>Corrosion Material 1:</strong>{dt.Corrosion_Material_1}</p>
                                          <p><strong>Corrosion Material 2:</strong>{dt.Corrosion_Material_2}</p>
                                          <p><strong>Environment Category ID:</strong>{dt.Environment_Category_ID}</p>
                                          <p><strong>Environment Category:</strong>{dt.Environment_Category}</p>
                                          <p><strong>Corrosion Fastener Specification:</strong>{dt.Corrosion_Fastener_Specification}</p>
                                        </div>
                                      )
                                    }
                                  }
                                })}</Typography>
                            </AccordionDetails>
                          </Accordion>
                        </Typography>
                      </AccordionDetails>
                    </Accordion>
                  </Typography>
                </AccordionDetails>
              </Accordion>
            </Typography>
          </AccordionDetails>
        </Accordion>
      </div>
    );
  }
}