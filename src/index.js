import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import GetRequest from './GetRequest';
import PostRequest from './PostRequest';
import LoginPage from './LoginPage';
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

const hist = createBrowserHistory(); //This is so we can use "back" for client routing


ReactDOM.render(
  <Router history={hist}>
<Switch>
<Route path="/LoginPage" component={LoginPage}/>
        <Route path="/GetRequest" component={GetRequest}/>
        <Route path="/PostRequest" component={PostRequest}/>
    <Redirect from="/" to="/LoginPage"  />
    </Switch>
    </Router>,
  document.getElementById('root')
);

