import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';

export default class LoginPage extends React.Component {

    handleSubmit(event) {
        event.preventDefault();
        this.props.history.push('/GetRequest')
      }
      handleSubmit1(event) {
        event.preventDefault();
         this.props.history.push('/PostRequest')
      }

render(){
    return (
    <div  className="padd50"><Container >
        <CssBaseline />
        <Box  pb={15}/><h1>Welcome!</h1>
<Button variant="contained" color="secondary" type="submit" onClick={(e) => this.handleSubmit(e)}>
  GET record
</Button> <CssBaseline />
<Button variant="contained" color="secondary" type="submit" onClick={(e) => this.handleSubmit1(e)}>
  POST record
</Button></Container>
    </div>
    );
}
}